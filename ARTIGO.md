# PLATAFORMA DE APRENDIZAGEM DE CONTROLE DE PROCESSOS BASEADO EM TECNOLOGIAS WEB

- **Aluno:** Samuel Martins Peres
- **Orientador:** Rafael Emerick
- **Instituição:** Instituto Federal do Espírito Santo

## Resumo

É notável grande necessidade de escolas e faculdades se adequarem ao modelo de educação a distância, e utilizarem plataformas que possibilitem o ensino, a disponibilização de exercícios e material didático, ou até mesmo aulas práticas a distância, utilizando algum software de simulação por exemplo. Tratando-se de aulas práticas, há limitação no uso de plantas didáticas reais para ensino de Controle de Processos, e faltam soluções disponíveis na núvem, livres de licenças de alto custo para ensino de controle que seja possível realizar testes em um processo virtualizado. Neste contexto, este trabalho traz o desenvolvimento de uma plataforma, como prova de conceito, que seja possível realizar a simulação de processos de controle por meio execução via rede, de uma malha de controle em processos computacionais que se comunicam através de Tecnologias Web.
 
## 1. Introdução

No processo de apredizagem de disciplinas técnicas em geral na Engenharia, como Controle de Processos Industriais por exemplo, a aula prática é parte importante do aprendizado. Nem todos os cursos possuem laboratórios com dispositivos reais para treinamento, considerando todas as matérias da grade curricular, devido a vários motivos como, preço elevado de equipamentos didáticos para laboratório, segurança dos alunos no manuseio das ferramentas e dos equipamentos, falta de pessoas qualificadas para manutenção e operação dos equipamentos didáticos. 

Diante da pandemia de Covid-19, tornou-se mandatório aulas no modelo à distância. O acesso a laboratórios presenciais está restrito, e várias instituições de ensino não possuem plataformas de ensino disponíveis em núvem para que seja possível realizar simulações práticas, substituindo aulas presenciais em laboratório. 

Existem soluções disponíveis no mercado que são normalmente utilizadas pelas faculdades em aulas práticas à distância, como por exemplo o uso de softwares licenciados ou gratuitos, mas que devem ser instaladas na máquina do usuário e não possuem interface amigável e pronta para o uso. Uma solução alternativa é o uso de um Laboratório Virtual utilizando Tecnologias Web. Com um Laboratório Virtual, alunos podem realizar simulações de um processo em casa com acesso a um computador e Internet, livre de licensas pagas e sem necessidade de instalar novos softwares em máquinas pessoais.

### 1.1 Objetivos

#### 1.1.1 Objetivo Geral

Construção de um Laboratório Virtual e Remoto para ensino a distância na disciplina de Controle de Processos e afins.

#### 1.1.2 Objetivos Específicos

- Desenvolver protótipo de um Laboratório na Web com simulação de malha de controle;
- Tornar protótipo acessível pela Web para alunos e professores do IFES em Núvem Computacional pública, ou privada, como prova de conceito operacional.

### 1.2 Resultados Esperados
Espera-se que, ao fim deste projeto, seja desenvolvido um protótipo de um Laboratório disponível na Web, onde seja possível aplicar a teoria de controle na prática com simulações.