const model = require('socket.io-client')('http://localhost:3060');

const server = require('http').createServer();
const io = require('socket.io')(server, {
    cors: {
        origin: '*'
    }
});
/*   cors: {        origin: "http://localhost:3000",        methods: ["GET", "POST"],        credentials: true    }    */


host = '0.0.0.0';
port = 3030;
server.listen(port, host, (err) => {
    if (err) return console.log('Error ocurred: ' + err);
    console.log(`Server is listening on ${host}:${port}`);
});

// ------------- VARIÁVEIS CONTROLADOR ------------- //
var processValue = 0;
var processTime = 0;
var controlValue = 0;
var setPoint = 5;
var loop = 'closed';
var lastError = 0;
var integral = 0;
var derivative = 0;
var error = 0;
var Kp = 3;
var Ki = 6; //Kp * 2;
var Kd = 1.5; //Kp * 1 / 2;
var dt = 0.1;
var ts = 0.1;
var disturbio = 0;
var controlMode = 1;
var saturationPositive = 20;
var saturationNegative = -20;
var sampleTime = 100;
var numerador = [1];
var denominador = [1, 2, 1];
var processController = null;

// ------------- CLIENT ------------- //
model.on('connect', () => {
    console.log('Client controller connected');

    model.on("read", (data) => {
        processValue = data.processValue;
        processTime = data.processTime;
        console.log('PV', processValue);
        console.log('Time', processTime);
        console.log('SC', controlValue);
        console.log('disturbio', disturbio);
        console.log('e', error);
        console.log('KP', Kp);
        console.log('KI', Ki);
        console.log('KD', Kd);
        console.log('SP', setPoint);
        console.log('Loop', loop);
        console.log('Limite Superior', saturationPositive);
        console.log('Limite Inferior', saturationNegative);
        console.log('Control Mode', controlMode);

        if (loop !== "open") {

            // Controlador PID
            error = setPoint - processValue;

            integral = integral + Ki * error * dt;
            derivative = Kd * (error - lastError) / dt;
            lastError = error;

            controlValue = (Kp * error) + integral + derivative;

            // Modo de controle
            //controlValue = controlMode * controlValue;

            // Disturbio
            controlValue = Number(controlValue) + Number(disturbio);

            // Limites de controle
            if (controlValue >= saturationPositive){
                controlValue = saturationPositive;
            } else if (controlValue <= saturationNegative){
                controlValue = saturationNegative;
            }

        } else {

            // Malha Aberta
            error = setPoint;
            controlValue = Number(error) + Number(disturbio);
        }

        var dataObject = {};
        dataObject = {
            controlValue: controlValue,
            numerador: numerador,
            denominador: denominador,
            ts: ts
        };

        model.emit('read', JSON.stringify(dataObject));
    });

    model.on("disconnect", reason => {
        console.log('Server disconnected', reason);
    });
});


// ------------- SERVER ------------- //
io.on('connection', socket => {
    console.log('Client connected to controller');

    socket.on("start", () => {
        console.log('Start simulation');
        model.emit('start');

        // -------- PROCESS CONTROLLER LOOP -------- //
        clearInterval(processController);
        processController = null;
        processController = setInterval(() => {

            socket.emit("read", {
                processValue: processValue,
                processTime: processTime,
                controlValue: controlValue,
                setPoint: setPoint
            });
        }, sampleTime);
    });

    socket.on("read", (data) => {
        data = JSON.parse(data);
        console.log(data);

        loop = data["loop"];
        setPoint = data["setpoint"];
        Kp = data["KP"];
        Ki = data["KI"];
        Kd = data["KD"];
        disturbio = data["disturbio"];
        numerador = data["numerador"];
        denominador = data["denominador"];
        saturationPositive = data["limitesuperior"];
        saturationNegative = data["limiteinferior"];
        controlMode = data["controlmode"];
    });

    socket.on("restart", () => {
        processValue = 0;
        processTime = 0;
        controlValue = 0;
        lastError = 0;
        integral = 0;
        error = 0;
        console.log('Restart process controller');
        try {
            model.emit('restart');
            clearInterval(processController);
            console.log('Stop process controller');
        } catch (error) {
            return 0;
        };
    });

    socket.on("stop", () => {
        try {
            model.emit('stop');
            clearInterval(processController);
            console.log('Stop process controller');
        } catch (error) {
            return 0;
        };
    });

    socket.on("disconnect", reason => {
        console.log('Client disconnected', reason);
        try {
            model.emit('stop');
            clearInterval(processController);
            console.log('Stop process controller');
        } catch (error) {
            return 0;
        };
    });
});