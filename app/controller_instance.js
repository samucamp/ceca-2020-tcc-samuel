const plantVirtual = require('socket.io-client')('http://localhost:3061');
//const plantReal = require('socket.io-client')('http://localhost:3090');

const server = require('http').createServer();
const io = require('socket.io')(server, {
    cors: {
        origin: '*'
    }
});
/*   cors: {        origin: "http://localhost:3000",        methods: ["GET", "POST"],        credentials: true    }    */


host = '0.0.0.0';
port = 3031;
server.listen(port, host, (err) => {
    if (err) return console.log('Error ocurred: ' + err);
    console.log(`Server is listening on ${host}:${port}`);
});

// ------------- VARIÁVEIS CONTROLADOR ------------- //
var processValue = 0;
var processTime = 0;
var controlValue = 0;
var setPoint = 1;
var loop = 'closed';
var lastError = 0;
var integral = 0;
var derivative = 0;
var error = 0;
var Kp = 1.9;
var Ki = 1; 
var Kd = 0.236; 
var dt = 0.1;
var disturbio = 0;
var saturationPositive = 10;
var saturationNegative = -10;
var sampleTime = 100;
var plant = null;
var processController = null;

// ------------- CLIENT VIRTUAL PLANT ------------- //
plantVirtual.on('connect', () => {
    console.log('Client controller connected');

    plantVirtual.on("read", (data) => {
        processValue = data.processValue;
        processTime = data.processTime;
        console.log('PV', processValue);
        console.log('Time', processTime);
        console.log('SC', controlValue);
        console.log('disturbio', disturbio);
        console.log('e', error);
        console.log('KP', Kp);
        console.log('KI', Ki);
        console.log('KD', Kd);
        console.log('SP', setPoint);
        console.log('Planta', plant);
        console.log('Loop', loop);
        console.log('Limite Superior', saturationPositive);
        console.log('Limite Inferior', saturationNegative);


        if (loop !== "open") {

            // Controlador PID
            error = setPoint - processValue;

            integral = integral + Ki * error * dt;
            derivative = Kd * (error - lastError) / dt;
            lastError = error;

            controlValue = (Kp * error) + integral + derivative;

            // Disturbio
            controlValue = Number(controlValue) + Number(disturbio);

            // Limites de controle
            if (controlValue >= saturationPositive){
                controlValue = saturationPositive;
            } else if (controlValue <= saturationNegative){
                controlValue = saturationNegative;
            }

        } else {

            // Malha Aberta
            error = setPoint;
            controlValue = Number(error) + Number(disturbio);
        }

        var dataObject = {};
        dataObject = {
            controlValue: controlValue,
        };

        plantVirtual.emit('read', JSON.stringify(dataObject));
    });

    plantVirtual.on("disconnect", reason => {
        console.log('Server disconnected', reason);
    });
});

// ------------- SERVER ------------- //
io.on('connection', socket => {
    console.log('Client connected to controller');

    socket.on("start", () => {
        console.log('Start simulation');
        plantVirtual.emit('start');
        //plantReal.emit('start');

        // -------- PROCESS CONTROLLER LOOP -------- //
        clearInterval(processController);
        processController = null;
        processController = setInterval(() => {

            socket.emit("read", {
                processValue: processValue,
                processTime: processTime,
                controlValue: controlValue,
                setPoint: setPoint
            });
        }, sampleTime);
    });

    socket.on("read", (data) => {
        data = JSON.parse(data);
        console.log(data);

        loop = data["loop"];
        plant = data["plant"];
        setPoint = data["setpoint"];
        Kp = data["KP"];
        Ki = data["KI"];
        Kd = data["KD"];
        disturbio = data["disturbio"];
        saturationPositive = data["limitesuperior"];
        saturationNegative = data["limiteinferior"];
    });

    socket.on("restart", () => {
        processValue = 0;
        processTime = 0;
        controlValue = 0;
        lastError = 0;
        integral = 0;
        error = 0;
        console.log('Restart process controller');
        try {
            plantVirtual.emit('restart');
            //plantReal.emit('restart');
            clearInterval(processController);
            console.log('Stop process controller');
        } catch (error) {
            return 0;
        };
    });

    socket.on("stop", () => {
        try {
            plantVirtual.emit('stop');
            //plantReal.emit('stop');
            clearInterval(processController);
            console.log('Stop process controller');
        } catch (error) {
            return 0;
        };
    });

    socket.on("disconnect", reason => {
        console.log('Client disconnected', reason);
        try {
            plantVirtual.emit('stop');
            //plantReal.emit('stop');
            clearInterval(processController);
            console.log('Stop process controller');
        } catch (error) {
            return 0;
        };
    });
});