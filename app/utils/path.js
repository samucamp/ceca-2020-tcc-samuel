const path = require('path');

// mainModule returns online_shop_project main module
module.exports = path.dirname(process.mainModule.filename);