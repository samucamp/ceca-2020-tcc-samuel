const axios = require('axios');

const stringfyRequest = (kp, ki, kd, numerador, denominador) => {
    const numParsed = [];
    const denParsed = [];
    const paramsParsed = [];

    const numSplited = numerador.split(',');
    const denSplited = denominador.split(',');

    numSplited.forEach(i => {
        numParsed.push(parseFloat(i))
    });

    denSplited.forEach(i => {
        denParsed.push(parseFloat(i))
    });

    paramsParsed.push(parseFloat(kd))
    paramsParsed.push(parseFloat(kp))
    paramsParsed.push(parseFloat(ki))

    const data = JSON.stringify({
        parametros: paramsParsed,
        numerador: numParsed,
        denominador: denParsed
    });

    // console.log(paramsParsed);
    // console.log(numParsed);
    // console.log(denParsed);

    return data;
};

module.exports = class Pid {
    constructor(kp, ki, kd, numerador, denominador) {
        this.kp = kp;
        this.ki = ki;
        this.kd = kd;
        this.numerador = numerador;
        this.denominador = denominador;
    };

    pid = (callback) => {
        const data = stringfyRequest(this.kp, this.ki, this.kd, this.numerador, this.denominador);
        axios({
            method: 'get',
            url: 'http://localhost:5000/pid',
            data: data
        }).then(res => {
            //console.log(res.data)
            callback(res.data)
        }).catch(err => console.log(err));
    };
};