const server = require('http').createServer();
const io = require('socket.io')(server, {});

/* TESTING ONLY, REMOVE WHEN PROD */
//const fs = require('fs');

host = '0.0.0.0'
port = 3061;
server.listen(port, host, (err) => {
    if (err) return console.log('Error ocurred: ' + err);
    console.log(`Server is listening on ${host}:${port}`);
});

var X = [0];
var Y = [0];
var timer = [0];
const sampleTime = 100;
var processPlant = null;

// ------- index para z-i -------//
function index(v, i) {
    try {
        return v[v.length + i] != undefined ? v[v.length + i] : 0;
    } catch (error) {
        return 0;
    }
}

// ------- SERVER -------//
io.on('connection', socket => {
    console.log('Client connected to plant');

    socket.on("start", () => {
        console.log('Start process plant');

        // -------- PROCESS PLANT LOOP -------- //
        clearInterval(processPlant);
        processPlant = null;
        processPlant = setInterval(() => {
            console.log('Gs - ', 'num = 0.56', 'den = 0.5s + 1');
            console.log('Gz - ', 'numz = 0.1015', 'denz = z - 0.8187', 'ts=0.1s');
            

            /* MODELO DO PROCESSO PRÉ DEFINIDO */
            Y.push(0.8187 * index(Y, -1)  + 0.1015 * index(X, -1))

            if (X.length < Y.length) {
                X.push(0);
            }

            timer.push(timer[timer.length - 1] + sampleTime / 1000);

            console.log('Running process plant');


            // TESTING PURPOSE ONLY
            // REMOVE THIS WHEN PRODUCTION
            /*  ******************  
            
                console.log('Time', timer)
                console.log('SC', X)
                console.log('VP', Y)

                if (timer[timer.length - 1] >= 20.0){
                    fs.writeFile('simul-SC.txt', X, function (err){
                        if (err) return console.log(err);
                        console.log('Simulação > simul-SC.txt');
                    });
                    fs.writeFile('simul-VP.txt', Y, function (err){
                        if (err) return console.log(err);
                        console.log('Simulação > simul-VP.txt');
                    });
                    fs.writeFile('simul-time.txt', timer, function (err){
                        if (err) return console.log(err);
                        console.log('Simulação > simul-timer.txt');
                    });
                }            
            ******************  */

            socket.emit("read", {
                processValue: Y[Y.length - 1],
                processTime: timer[timer.length - 1]
            });

        }, sampleTime);
    });

    socket.on("read", data => {
        data = JSON.parse(data);
        console.log(data);

        X.push(Number(data.controlValue));
    });

    socket.on("restart", () => {
        X = [0];
        Y = [0];
        timer = [0];
        console.log('Restart process plant');
        try {
            clearInterval(processPlant);
            console.log('Stop process plant');
        } catch (error) {
            return 0;
        };
    });

    socket.on("stop", () => {
        try {
            clearInterval(processPlant);
            console.log('Stop process plant');
        } catch (error) {
            return 0;
        };
    });

    socket.on("disconnect", reason => {
        console.log('Client disconnected', reason);
        try {
            clearInterval(processPlant);
            console.log('Stop process plant');
        } catch (error) {
            return 0;
        };
    });
});