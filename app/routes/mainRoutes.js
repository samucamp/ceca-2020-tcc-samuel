const express = require('express');

const appController = require('../controllers/mainController');

const router = express.Router();

// / => GET
router.get('/', appController.getHome);

// /article => GET
router.get('/article', appController.getArticle);

// /theory => GET
//router.get('/theory', appController.getTheory);

// /control => GET
//router.get('/control', appController.getControl);

// /documentation => GET
router.get('/documentation', appController.getDocumentation);

// /about => GET
//router.get('/about', appController.getAbout);


module.exports = router;