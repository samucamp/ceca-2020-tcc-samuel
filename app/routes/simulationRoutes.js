const express = require('express');

const appController = require('../controllers/simulationController');

const router = express.Router();


// /simulation => GET
router.get('/', appController.getSimulation);

// /simulation/generic => GET
router.get('/generic', appController.getGenericSimulation);

// /simulation/level => GET
router.get('/level', appController.getLevelSimulation);

// /simulation/level => GET
//router.get('/real', appController.getRealSimulation);

// /simulation/step => GET
//router.get('/step', appController.getStepSimulation);

// /simulation/step => POST
//router.post('/step', appController.postStepSimulation);

// /simulation/pid => GET
//router.get('/pid', appController.getPidSimulation);

// /simulation/pid => POST
//router.post('/pid', appController.postPidSimulation);


module.exports = router;