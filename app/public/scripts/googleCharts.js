// ------------- GOOGLE CHART ------------- //
$(document).ready(function () {
    pid_form = $('form.pid');
    step_form = $('form.step');

    step_form.on('submit', function () {
        $.post('/simulation/step', $(this).serialize(), function (response) {
            console.log(response);
            const dataToPlot = prepareResponseToPlot(response);
            googleChartsPlot(dataToPlot);
        });
        return false;
    });

    pid_form.on('submit', function () {
        $.post('/simulation/pid', $(this).serialize(), function (response) {
            console.log(response);
            const dataToPlot = prepareResponseToPlot(response);
            googleChartsPlot(dataToPlot);
        });
        return false;
    });
});

function prepareResponseToPlot(response) {
    const X = response.xaxis;
    const Y = response.yaxis;
    const dataToPlot = [];
    for (let i of X) {
        const aux = [];
        aux.push(X[X.indexOf(i)]);
        aux.push(Y[X.indexOf(i)]);
        dataToPlot.push(aux);
    };
    return dataToPlot;
};

function googleChartsPlot(dataToPlot) {
    google.charts.load('current', {
        packages: ['corechart', 'line']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('number', 'X');
        data.addColumn('number', 'Step');

        data.addRows(dataToPlot);

        var options = {
            hAxis: {
                title: 'Time'
            },
            vAxis: {
                title: 'Process Value'
            },
            backgroundColor: '#f1f8e9'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    };
};
