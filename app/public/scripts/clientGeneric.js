/* ALTERAR CONFORME SERVIDOR UTILIZADO */
const controller = io('http://localhost:3030');
//const controller = io('http://localhost:3030');
/* ALTERAR CONFORME SERVIDOR UTILIZADO */

const startProcess = $('form.startProcess');
const stopProcess = $('form.stopProcess');
const restartProcess = $('form.restartProcess');

var processTime = [0];
var processValue = [0];
var controlValue = [0];
var setPoint = [0];

// ------------- CLIENT ------------- //
controller.on('connect', () => {
    console.log('Client chart connected');
  
    startProcess.on('submit', function () {
            controller.emit('start');

            var dataObject = {};

            dataArray = $(this).serializeArray();
            dataArray.forEach(element => {
                dataObject[element.name] = element.value;
            });
            
            controller.emit('read', JSON.stringify(dataObject));   
            return false;
    });
    
    controller.on("read", (data) => {
        addData(myChart, 2, data.setPoint);
        addData(myChart, 1, data.controlValue);
        addDataPlant(myChart, data.processTime.toFixed(2), data.processValue);
    });

    restartProcess.on('submit', function () {
        removeData(myChart);
        controller.emit('restart');
        return false;
    });

    stopProcess.on('submit', function () {
        controller.emit('stop');
        return false;
    });
    
    controller.on("disconnect", reason => {
        console.log('Server disconnected', reason);
    });
});