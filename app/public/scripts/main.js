$(document).ready(function () {
    body = $('body');

    body.on('click', '#step', function () {
        location.href = "/simulation/generic";
    })

    body.on('click', '#pid', function () {
        location.href = "/simulation/level";
    })

    body.on('click', '#real', function () {
        location.href = "/simulation/real";
    })

    $("#setpoint, #KP, #KI, #KD, #disturbio, #numerador, #denominador, #limiteinferior, #limitesuperior, #controlmode").show();

    $("input:radio[name=loop]").on("change", function () {
        if ($(this).val() == "open") {
            $("#setpoint, #disturbio, #numerador, #denominador").show();
            $("#KP, #KI, #KD, #limiteinferior, #limitesuperior, #controlmode").hide();
        } else if ($(this).val() == "closed") {
            $("#setpoint, #KP, #KI, #KD, #disturbio, #numerador, #denominador, #limiteinferior, #limitesuperior, #controlmode").show();
        }
    });
});