let h = Math.sqrt(3) / 2;

function tankBack(tank) {

  fill(150, tank.t_opacity);
  stroke(100);

  // rear left
  beginShape();
  vertex(tank.x, tank.y);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2 + tank.h);
  vertex(tank.x, tank.y + tank.h);
  endShape(CLOSE);

  // rear right
  beginShape();
  vertex(tank.x, tank.y);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2 + tank.h);
  vertex(tank.x, tank.y + tank.h);
  endShape(CLOSE);

  // rear left small
  beginShape();
  vertex(tank.x, tank.y + tank.thickness);
  vertex(tank.x - tank.w * h + tank.thickness * h * 2, tank.y + tank.w / 2);
  vertex(tank.x - tank.w * h + tank.thickness * h * 2, tank.y + tank.w / 2 + tank.h - tank.thickness * h);
  vertex(tank.x, tank.y + tank.thickness + tank.h - tank.thickness * h);
  endShape(CLOSE);

  // rear right small
  beginShape();
  vertex(tank.x, tank.y + tank.thickness);
  vertex(tank.x + tank.w * h - tank.thickness * h * 2, tank.y + tank.w / 2);
  vertex(tank.x + tank.w * h - tank.thickness * h * 2, tank.y + tank.w / 2 + tank.h - tank.thickness * h);
  vertex(tank.x, tank.y + tank.thickness + tank.h - tank.thickness * h);
  endShape(CLOSE);
}

function tankLeakFree(tank, frame, speed) {
  // water leak
  let pourLX = tank.x - tank.holeW * h;
  let pourRX = tank.x + tank.holeW * h;
  let pourWaterY = tank.y + tank.h + tank.w / 2 - tank.thickness;
  let blockHeight = sqrt(tank.waterH);
  let pourY = pourWaterY + frame % speed * blockHeight / speed;

  fill(110, 210, 255, tank.w_opacity);
  noStroke();

  beginShape();
  vertex(pourRX, pourY-1);
  vertex(tank.x, pourY-1 + tank.holeW / 2);
  vertex(pourLX, pourY-1);
  let floorPourY = max(pourWaterY,pourY-1-blockHeight);
  vertex(pourLX, floorPourY);
  vertex(tank.x, floorPourY + tank.holeW / 2);
  vertex(pourRX, floorPourY);
  endShape(CLOSE);

  while (pourY < 600) {
    beginShape();
    vertex(pourLX, pourY);
    vertex(tank.x, pourY + tank.holeW / 2);
    vertex(pourRX, pourY);
    pourY = min(pourY + blockHeight, 610);
    vertex(pourRX, pourY);
    vertex(tank.x, pourY + tank.holeW / 2);
    vertex(pourLX, pourY);
    endShape(CLOSE);
    pourY = pourY + 1;
  }
}

function tankLeakPump(tank, frame, speed) {
  // water leak
  let pourLX = tank.x - tank.holeW * h;
  let pourRX = tank.x + tank.holeW * h;
  let pourWaterY = tank.y + tank.h + tank.w / 2 - tank.thickness;
  let blockHeight = 10;
  let pourY = pourWaterY + frame % speed * blockHeight / speed;

  fill(110, 210, 255, tank.w_opacity);
  noStroke();

  beginShape();
  vertex(pourRX, pourY-1);
  vertex(tank.x, pourY-1 + tank.holeW / 2);
  vertex(pourLX, pourY-1);
  let floorPourY = max(pourWaterY,pourY-1-blockHeight);
  vertex(pourLX, floorPourY);
  vertex(tank.x, floorPourY + tank.holeW / 2);
  vertex(pourRX, floorPourY);
  endShape(CLOSE);

  while (pourY < 600) {
    beginShape();
    vertex(pourLX, pourY);
    vertex(tank.x, pourY + tank.holeW / 2);
    vertex(pourRX, pourY);
    pourY = min(pourY + blockHeight, 610);
    vertex(pourRX, pourY);
    vertex(tank.x, pourY + tank.holeW / 2);
    vertex(pourLX, pourY);
    endShape(CLOSE);
    pourY = pourY + 1;
  }

  fill(10, tank.t_opacity * 2);
  stroke(100);

  // pipe/pump
  beginShape();
  vertex(pourLX,pourWaterY);
  vertex(tank.x,pourWaterY - tank.holeW / 2);
  vertex(pourRX,pourWaterY);
  vertex(pourRX,610);
  vertex(pourLX,610);
  endShape(CLOSE);
  line(tank.x,pourWaterY - tank.holeW / 2,tank.x,610);
}

function tankBottomTop(tank) {

  fill(150, tank.t_opacity);
  stroke(100);

  // bottom
  beginShape();
  vertex(tank.x, tank.y + tank.h);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2 + tank.h);
  vertex(tank.x, tank.y + tank.w + tank.h);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2 + tank.h);
  beginContour();
  vertex(tank.x, tank.y + tank.h + tank.w / 2 - tank.holeW / 2);
  vertex(tank.x - tank.holeW * h, tank.y + tank.h + tank.w / 2);
  vertex(tank.x, tank.y + tank.h + tank.w / 2 + tank.holeW / 2);
  vertex(tank.x + tank.holeW * h, tank.y + tank.h + tank.w / 2);
  endContour();
  endShape(CLOSE);

  // upper bottom
  beginShape();
  vertex(tank.x, tank.y + tank.h);
  vertex(tank.x + tank.w * h - tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness);
  vertex(tank.x, tank.y + tank.w + tank.h - tank.thickness * 2);
  vertex(tank.x - tank.w * h + tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness);
  beginContour();
  vertex(tank.x, tank.y + tank.h + tank.w / 2 - tank.holeW / 2 - tank.thickness);
  vertex(tank.x - tank.holeW * h, tank.y + tank.h + tank.w / 2 - tank.thickness);
  vertex(tank.x, tank.y + tank.h + tank.w / 2 + tank.holeW / 2 - tank.thickness);
  vertex(tank.x + tank.holeW * h, tank.y + tank.h + tank.w / 2 - tank.thickness);
  endContour();
  endShape(CLOSE);

  line(tank.x - tank.holeW * h, tank.y + tank.h + tank.w / 2, tank.x - tank.holeW * h, tank.y + tank.h + tank.w / 2 - tank.thickness);
  line(tank.x + tank.holeW * h, tank.y + tank.h + tank.w / 2, tank.x + tank.holeW * h, tank.y + tank.h + tank.w / 2 - tank.thickness);

  // top
  beginShape();
  vertex(tank.x, tank.y);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2);
  vertex(tank.x, tank.y + tank.w);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2);
  beginContour();
  vertex(tank.x, tank.y + tank.thickness);
  vertex(tank.x - tank.w * h + tank.thickness * h * 2, tank.y + tank.w / 2);
  vertex(tank.x, tank.y + tank.w - tank.thickness);
  vertex(tank.x + tank.w * h - tank.thickness * h * 2, tank.y + tank.w / 2);
  endContour();
  endShape(CLOSE);
}

function tankPour(tank, frame, speed) {
  // water pour
  fill(110, 210, 255, tank.w_opacity);
  let pourLX = tank.x - tank.pourW * h;
  let pourRX = tank.x + tank.pourW * h;
  tank.pourWaterY = tank.y + tank.h + tank.w / 2 - tank.thickness - tank.waterH;
  let blockHeight = 10;
  let pourY = -20 + frame % speed * blockHeight / speed;

  while (pourY < tank.pourWaterY) {
    beginShape();
    vertex(pourLX, pourY);
    vertex(tank.x, pourY + tank.pourW / 2);
    vertex(pourRX, pourY);
    pourY = min(pourY + blockHeight, tank.pourWaterY);
    vertex(pourRX, pourY);
    vertex(tank.x, pourY + tank.pourW / 2);
    vertex(pourLX, pourY);
    endShape(CLOSE);
    pourY = pourY + 1;
  }
}

function tankPourSpout(tank) {
  fill(100,255);
  beginShape();
  endShape(CLOSE);
}

function tankWater(tank) {

  fill(110, 210, 255, tank.w_opacity);
  noStroke();

  // water top
  beginShape();
  vertex(tank.x, tank.y + tank.h - tank.waterH);
  vertex(tank.x + tank.w * h - tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness - tank.waterH);
  vertex(tank.x, tank.y + tank.w + tank.h - tank.thickness * 2 - tank.waterH);
  vertex(tank.x - tank.w * h + tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness - tank.waterH);
  endShape(CLOSE);

  fill(100, 200, 250, tank.w_opacity);

  // water left
  beginShape();
  vertex(tank.x, tank.y + tank.w + tank.h - tank.thickness * 2 - tank.waterH);
  vertex(tank.x - tank.w * h + tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness - tank.waterH);
  vertex(tank.x - tank.w * h + tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness);
  vertex(tank.x, tank.y + tank.w + tank.h - tank.thickness * 2);
  endShape(CLOSE);

  // water right
  beginShape();
  vertex(tank.x, tank.y + tank.w + tank.h - tank.thickness * 2 - tank.waterH);
  vertex(tank.x + tank.w * h - tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness - tank.waterH);
  vertex(tank.x + tank.w * h - tank.thickness * 2 * h, tank.y + tank.w / 2 + tank.h - tank.thickness);
  vertex(tank.x, tank.y + tank.w + tank.h - tank.thickness * 2);
  endShape(CLOSE);
}

function tankFront(tank) {
  fill(150, tank.t_opacity);
  stroke(100);

  // front left
  beginShape();
  vertex(tank.x, tank.y + tank.w);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2 + tank.h);
  vertex(tank.x, tank.y + tank.w + tank.h);
  endShape(CLOSE);

  // front right
  beginShape();
  vertex(tank.x, tank.y + tank.w);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2 + tank.h);
  vertex(tank.x, tank.y + tank.w + tank.h);
  endShape(CLOSE);

  // front left small
  beginShape();
  vertex(tank.x, tank.y + tank.w - tank.thickness);
  vertex(tank.x - tank.w * h + tank.thickness * h * 2, tank.y + tank.w / 2);
  vertex(tank.x - tank.w * h + tank.thickness * h * 2, tank.y + tank.w / 2 + tank.h - tank.thickness * h);
  vertex(tank.x, tank.y + tank.w - tank.thickness + tank.h - tank.thickness * h);
  endShape(CLOSE);

  // front right small
  beginShape();
  vertex(tank.x, tank.y + tank.w - tank.thickness);
  vertex(tank.x + tank.w * h - tank.thickness * h * 2, tank.y + tank.w / 2);
  vertex(tank.x + tank.w * h - tank.thickness * h * 2, tank.y + tank.w / 2 + tank.h - tank.thickness * h);
  vertex(tank.x, tank.y + tank.w - tank.thickness + tank.h - tank.thickness * h);
  endShape(CLOSE);
}

function tankOverflow(tank) {
  fill(110, 210, 255, tank.w_opacity);
  noStroke();

  beginShape();
  vertex(tank.x, tank.y);
  vertex(tank.x - tank.w * h, tank.y + tank.w / 2);
  vertex(tank.x - tank.w * h, 610);
  vertex(tank.x + tank.w * h, 610);
  vertex(tank.x + tank.w * h, tank.y + tank.w / 2);
  endShape(CLOSE);
}

function drawTank(tankW, tankH, waterLevel, holeArea, pourArea, overflow, hasPump, frame) {
  let tank = {};

  tank.x = 250;
  tank.y = 100;

  tank.thickness = 5;
  tank.w = tankW + tank.thickness * 2;
  tank.h = tankH + tank.thickness;

  tank.waterH = waterLevel;
  tank.holeW = sqrt(holeArea);
  tank.pourW = sqrt(pourArea);

  tank.t_opacity = 20;
  tank.w_opacity = 200;

  let speed = 30;

  tankBack(tank);

  if (tank.waterH > 0 && tank.holeW > 0) {
    if (hasPump) {
      tankLeakPump(tank, frame, speed);
    } else {
      tankLeakFree(tank, frame, speed);
    }
  }

  tankBottomTop(tank);

  if (tank.waterH > 0) {
    tankWater(tank);

    if (tank.pourW > 0) {
      tankPour(tank, frame, speed);
    }
  }

  tankPourSpout(tank);

  tankFront(tank)

  if (overflow) {
    tankOverflow(tank);
  }

}
