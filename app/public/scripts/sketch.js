let tankW = 150; // units
let tankH = 200;

let waterLevel = 100; // square units
let holeArea = 0;

let inputFlowRate = 500;
let outputFlowRate = 200;

let slider;

function setup() {
  createCanvas(500, 600);
  slider = createSlider(0,700,100,10);
}

function draw() {
  background(255);
  
  holeArea = slider.value();
  
  let hasPump = false;
  
  if (frameCount % 3 === 0) {
    if (hasPump) {
      outputFlowRate = holeArea;
    } else {
      outputFlowRate = 0.5 * holeArea * sqrt(waterLevel);
    }
    
    waterLevel = waterLevel + inputFlowRate/100 - outputFlowRate/100;
  }
    
  // overflow
  let overflow = waterLevel >= tankH && inputFlowRate > 0;
  
  waterLevel = min(tankH,waterLevel);
  waterLevel = max(0,waterLevel);
  let pourArea = inputFlowRate;
  drawTank(tankW,tankH,waterLevel,holeArea,pourArea,overflow,hasPump,frameCount);
}