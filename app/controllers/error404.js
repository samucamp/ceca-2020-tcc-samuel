exports.get404 = (req, res, next) => {
    res.status(404).render('templates/error404', {
        pageTitle: 'Page Not Found',
        path: '/404'
    });
};