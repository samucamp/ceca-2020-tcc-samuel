//const Step = require('../models/stepResponse')
//const Pid = require('../models/pidResponse')

exports.getSimulation = (req, res) => {
    res.render('pages/simulation', {
        pageTitle: 'Samulabs - Simulação',
        path: '/simulation'
    });
};

exports.getGenericSimulation = (req, res) => {
    res.render('pages/simulation_generic', {
        pageTitle: 'Samulabs - Simulation',
        path: '/simulation/generic'
    });
};

exports.getLevelSimulation = (req, res) => {
    res.render('pages/simulation_level', {
        pageTitle: 'Samulabs - Simulation',
        path: '/simulation/level'
    });
};
/*
exports.getRealSimulation = (req, res) => {
    res.render('pages/simulation_real', {
        pageTitle: 'Samulabs - Remote',
        path: '/simulation/real'
    });
};
*/

/*
exports.getStepSimulation = (req, res) => {
    res.render('pages/simulation_step', {
        pageTitle: 'Samulabs - Step',
        path: '/simulation/step'
    });
};

exports.postStepSimulation = (req, res) => {
    console.log('Received request from client: ')
    console.log('Numerador: ' + req.body.numerador, '\nDenominador: ' + req.body.denominador)
    const numerador = req.body.numerador;
    const denominador = req.body.denominador;
    const test = new Step(numerador, denominador);
    test.step(
        (callbackResponse) => {
            console.log('Sending response to client');
            res.send(callbackResponse);
        }
    );
}

exports.getPidSimulation = (req, res) => {
    res.render('pages/simulation_pid', {
        pageTitle: 'Samulabs - PID',
        path: '/simulation/pid'
    });
};

exports.postPidSimulation = (req, res) => {
    console.log('Received request from client: ')
    console.log('Kp: ' + req.body.kp, '\nKi: ' + req.body.ki, '\nKd: ' + req.body.kd)
    console.log('Numerador: ' + req.body.numerador, '\nDenominador: ' + req.body.denominador)
    const kp = req.body.kp;
    const ki = req.body.ki;
    const kd = req.body.kd;
    const numerador = req.body.numerador;
    const denominador = req.body.denominador;
    const test = new Pid(kp, ki, kd, numerador, denominador);
    test.pid(
        (callbackResponse) => {
            console.log('Sending response to client');
            res.send(callbackResponse);
        }
    );
}
*/