exports.getHome = (req, res) => {
    res.render('pages/home', {
        pageTitle: 'Samulabs - Home',
        path: '/'
    });
};

exports.getArticle = (req, res) => {
    res.render('pages/article', {
        pageTitle: 'Samulabs - Artigo',
        path: '/article'
    });
};

exports.getTheory = (req, res) => {
    res.render('templates/construction', {
        pageTitle: 'Samulabs - Teoria',
        path: '/theory'
    });
};

exports.getControl = (req, res) => {
    res.render('pages/control', {
        pageTitle: 'Samulabs - Planta',
        path: '/control'
    });
};

exports.getDocumentation = (req, res) => {
    res.render('pages/documentation', {
        pageTitle: 'Samulabs - Documentação',
        path: '/documentation'
    });
};

exports.getAbout = (req, res) => {
    res.render('templates/construction', {
        pageTitle: 'Samulabs - Sobre',
        path: '/about'
    });
};