const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');

const errorController = require('./controllers/error404');

const mainRoutes = require('./routes/mainRoutes');
const simulationRoutes = require('./routes/simulationRoutes')

const {
    port,
    host
} = require('./utils/config');

const app = express();

// view engine and path
app.set('view engine', 'ejs');
app.set('views', 'views');

// assets compression
app.use(compression());

// body parser forms urlencoded
app.use(bodyParser.urlencoded({
    extended: false //not allow nested objects
}));

// serving static files
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use('/simulation', simulationRoutes)
app.use(mainRoutes);

// middleware for error 404
app.use(errorController.get404);

app.listen(port, host, (err) => {
    if (err) return console.log('Error ocurred: ' + err);
    console.log(`Server is running in ${host}:${port}`);
});