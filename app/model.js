const server = require('http').createServer();
const io = require('socket.io')(server, {});

/* TESTING ONLY, REMOVE WHEN PROD */
//const fs = require('fs');


const axios = require('axios');

host = '0.0.0.0'
port = 3060;
server.listen(port, host, (err) => {
    if (err) return console.log('Error ocurred: ' + err);
    console.log(`Server is listening on ${host}:${port}`);
});

var X = [0];
var Y = [0];
var timer = [0];
const sampleTime = 100;
var processModel = null;
var numerador = '1';
var denominador = '1, 2, 1';
var numz = [0, 0.004679, 0.004377];
var denz = [1, -1.81, 0.8187];
var ts = 0.1;

// ------- index para z-i -------//
function index(v, i) {
    try {
        return v[v.length + i] != undefined ? v[v.length + i] : 0;
    } catch (error) {
        return 0;
    }
}

// ------- SERVER -------//
io.on('connection', socket => {
    console.log('Client connected to plant');

    socket.on("start", () => {
        console.log('Start process plant');

        // -------- PROCESS PLANT LOOP -------- //
        clearInterval(processModel);
        processModel = null;
        processModel = setInterval(() => {
            console.log('PLANTA - ', numz, denz, ts);

            while (numz.length < 6) {
                numz.push(0);
            }

            while (denz.length < 6) {
                denz.push(0);
            }

            //Y.push(1.783 * index(Y, -1) - 0.8187 * index(Y, -2) + 0.004667 * index(X, -1) + 0.004374 * index(X, -2))
            Y.push((-denz[1] * index(Y, -1)) + (-denz[2] * index(Y, -2)) + (-denz[3] * index(Y, -3)) + (-denz[4] * index(Y, -4)) + numz[0] + (numz[1] * index(X, -1)) + (numz[2] * index(X, -2)) + (numz[3] * index(X, -3)) + (numz[4] * index(X, -4)) + (numz[5] * index(X, -5)))

            if (X.length < Y.length) {
                X.push(0);
            }

            timer.push(timer[timer.length - 1] + sampleTime / 1000);

            console.log('Running process plant');


            // TESTING PURPOSE ONLY
            // REMOVE THIS WHEN PRODUCTION
            /*  ******************  
            
                console.log('Time', timer)
                console.log('SC', X)
                console.log('VP', Y)

                if (timer[timer.length - 1] >= 20.0){
                    fs.writeFile('simul-SC.txt', X, function (err){
                        if (err) return console.log(err);
                        console.log('Simulação > simul-SC.txt');
                    });
                    fs.writeFile('simul-VP.txt', Y, function (err){
                        if (err) return console.log(err);
                        console.log('Simulação > simul-VP.txt');
                    });
                    fs.writeFile('simul-time.txt', timer, function (err){
                        if (err) return console.log(err);
                        console.log('Simulação > simul-timer.txt');
                    });
                }            
            ******************  */

            socket.emit("read", {
                processValue: Y[Y.length - 1],
                processTime: timer[timer.length - 1]
            });

        }, sampleTime);
    });

    socket.on("read", data => {
        data = JSON.parse(data);
        console.log(data);

        if ((data.numerador != numerador) || (data.denominador != denominador) || (data.ts != ts)) {

            numerador = data.numerador;
            denominador = data.denominador;
            ts = data.ts;

            var dataObject = {};
            dataObject = {
                numerador: numerador,
                denominador: denominador,
                ts: ts
            };

            console.log('CHANGED - ', dataObject);

            // send a GET request
            axios({
                method: 'get',
                url: 'http://127.0.0.1:5000/ztransform',
                data: JSON.stringify(dataObject)
            }).then((response) => {
                console.log('RESPONSE - ', response.data);
                numz = response.data.numerador;
                denz = response.data.denominador;
                ts = response.data.ts;
            }, (error) => {
                console.log(error);
            });
        }

        X.push(Number(data.controlValue));
    });

    socket.on("restart", () => {
        X = [0];
        Y = [0];
        timer = [0];
        console.log('Restart process plant');
        try {
            clearInterval(processModel);
            console.log('Stop process plant');
        } catch (error) {
            return 0;
        };
    });

    socket.on("stop", () => {
        try {
            clearInterval(processModel);
            console.log('Stop process plant');
        } catch (error) {
            return 0;
        };
    });

    socket.on("disconnect", reason => {
        console.log('Client disconnected', reason);
        try {
            clearInterval(processModel);
            console.log('Stop process plant');
        } catch (error) {
            return 0;
        };
    });
});