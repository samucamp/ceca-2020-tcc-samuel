# CECA 2020 - TCC - Samuel

- **Tema:** PLATAFORMA DE APRENDIZAGEM DE CONTROLE DE PROCESSOS BASEADO EM TECNOLOGIAS WEB

* **Aluno:** Samuel Martins Peres
* **Orientador:** Rafael Emerick
* **Instituição:** Instituto Federal do Espírito Santo

## Sobre

Este repositório será utilizado para guardar todos códigos e arquivos utilizados no Trabalho de Conclusão de Curso.

Favor verificar resumo e introdução sobre o trabalho em [ARTIGO](https://gitlab.com/samucamp/ceca-2020-tcc-samuel/-/blob/master/ARTIGO.md).

## Guia de Instalação e Inicialização

Favor verificar [CONTRIBUTING](https://gitlab.com/samucamp/ceca-2020-tcc-samuel/-/blob/master/CONTRIBUTING.md) detalhes de como proceder para rodar o projeto em sua máquina local.

## Authors

- **Samuel Martins Peres** - [samucamp](https://gitlab.com/samucamp/)

## License

All Rights Reserved

Copyright © **2020 - SamuLabs**

Check [LICENSE](https://gitlab.com/samucamp/ceca-2020-tcc-samuel/-/blob/master/LICENSE)
