from scipy import signal
import json
from control.matlab import *


def ztransform(numerador, denominador, dt=0.1):
    numc = []
    denc = []
    numv = []
    denv = []

    numc = numerador.strip("[").strip("]").split(',')
    denc = denominador.strip("[").strip("]").split(',')

    for i in numc:
        numv.append(float(i))

    for i in denc:
        denv.append(float(i))

    print(numv)
    print(denv)
    print(type(numv))

    numd, dend, ts = signal.cont2discrete(
        (numv, denv), dt, method='zoh', alpha=None)

    sys = TransferFunction(numv, denv)
    print(sys)
    sysd = sys.sample(dt, method='zoh')
    print(sysd)

    # a Python object (dict):
    response = {
        "numerador": numd.tolist()[0],
        "denominador": dend.tolist(),
        "ts": ts
    }

    print(response)

    # convert into JSON:
    json_dump = json.dumps(response)
    return json_dump
