from flask import Flask, request
from models.z_transform import ztransform as z
import json

app = Flask(__name__)

@app.route('/')
def home():
    return '<h1>Home</h1>'

@app.route("/ztransform")
def ztransform():
    y = json.loads(request.data)
    x = z(y["numerador"], y["denominador"], y["ts"])
    return x

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
    # debug=True is set to allow possible errors to appear on the web page.
    # IMPORTANT POINT: This shall never be used in production environment
