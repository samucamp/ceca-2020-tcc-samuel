## Instalação e inicialização

Verificar as instruções no guia para instalação do projeto em sua máquina. Obs.: O guia abaixo foi feito para distro UBUNTU 18.04, ou mais recente.

Para conseguir entender e replicar passos deste guia, é necessário conhecimento básico de Linux, Node, NPM, Python, pip. Mais informações sobre estes assuntos pode ser encontrando em [samuel-2020-linux-ubuntu](https://gitlab.com/samucamp/samuel-2020-linux-ubuntu), [samuel-2020-javascript-node](https://gitlab.com/samucamp/samuel-2020-javascript-node), [samuel-2020-python](https://gitlab.com/samucamp/samuel-2020-python).

## Guia

- GITLAB - git clone respositório

### Instalações e pre-requisitos

- Install Nodejs e NPM

```bash
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
node -v
npm -v
```

- Install Python, pip e bibliotecas

```bash
sudo apt install python3.8
sudo apt install python3-pip
python3.8 --version
pip3 --version
pip3 install -r ./api/requirements.txt --user
```

- (OBS.: caso não consiga instalar as bibliotecas através de requirements, basta ler o arquivo requirements e instalar individualmente.

- (OPCIONAL - ambiente isolado para instalar dependencias) Python Venv.

```bash
sudo apt-get install python3-venv
pip3 install --user virtualenv
python3 -m venv ./api/.venv
source ./api/.venv/bin/activate
pip3 install -r ./api/requirements.txt
```

Sempre que for rodar o projeto utilizando virtualenv deve lembrar de ativar o ambiente com comando source antes de iniciar a API. Para desativar basta utilizar deactivate.

```bash
source ./api/.venv/bin/activate
deactivate
```

### Iniciando a aplicação

Na arquivo /app/utils/config.js contém uso de uma biblioteca para gerenciar variáveis do ambiente. Para funcionar corretamente crie um arquivo .env em /app e cole os dados a seguir. (Obs.: API_KEY e API_URL ainda não está em uso).

```bash
echo 'PORT=3000
HOST=127.0.0.1
API_KEY=**************************
API_URL=**************************' > ./app/.env
```

Deverá ser utilizado dois terminais para rodar a aplicação inteira. Instale as dependências do package.json e rode a aplicação com NPM Scripts.

- Terminal para API (seja com venv ou local):

```bash
cd ./api
npm i
npm start
```

- Terminal para APP :

```bash
cd ./app
npm i
npm run all
```

Aplicação estará rodando em localhost:3000/

## Note

By contributing to this project, you agree to the terms listed in the [LICENSE](https://gitlab.com/samucamp/ceca-2020-tcc-samuel/-/blob/master/LICENSE)
[LICENSE](https://gitlab.com/samucamp/ceca-2020-tcc-samuel/-/blob/master/LICENSE)